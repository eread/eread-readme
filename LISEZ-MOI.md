<!-- vale Vale.Spelling = NO -->

# Le LISEZ-MOI d'Evan Read

Ce LISEZ-MOI vous offre des informations sur moi et sur mon environnement de développement.

- Voir la [version publiée](https://eread.gitlab.io/eread/fr/).
- Voir la [version Markdown](https://gitlab.com/eread/eread/-/tree/main/content/fr).

La documentation du projet [est disponible](./CONTRIBUTING.md#view-the-project-menu)
par un environnement en mode texte.

[Read in English](./README.md).
