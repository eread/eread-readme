---
title: 'About me'
showTableOfContents: true
summary: 'An experienced technical writer.'
tags:
  - 'me / moi'
---
<!-- vale french.french-lists-colons = NO -->
An experienced **technical** writer.

- My GitLab profile: [`eread`](https://gitlab.com/eread).
- My GitLab team member profile: [Evan Read](https://about.gitlab.com/company/team/#eread).
- My job description: [Senior Technical Writer](https://handbook.gitlab.com/job-families/product/technical-writer/#senior-technical-writer).

<div id="remove-from-pdf">
  {{< button href="about-evan-read.pdf" target="_self" >}}
  Download full <strong>About me</strong> section as a PDF
  {{< /button >}}
</div>

## My interests

Outside of work, I'm interested in:

- History.
- Languages (French in particular).
- Australian rules football.

## More about me

For more information about me, see the following sections.
