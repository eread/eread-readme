---
title: 'README of Evan Read'
---

{{< typeit
  speed=50
  lifeLike=true
  waitUntilVisible=true
>}}
An experienced <strong>technical</strong> writer.
{{< /typeit >}}

Read information [about me]({{< ref "about-me.md" >}}) and
[about my development setup]({{< ref "development-setup.md" >}}).
