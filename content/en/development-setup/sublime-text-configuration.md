---
title: 'Sublime Text configuration'
showTableOfContents: true
summary: 'My main Sublime Text configuration.'
tags:
  - 'dev / dév'
---

<!-- vale french.french-lists-colons = NO -->

I have custom Sublime Text configuration.

## Configuration for Sublime Text

Configuration for the main application, including
[settings for MarkdownEditing](https://github.com/SublimeText-Markdown/MarkdownEditing/blob/master/Preferences.sublime-settings):

```json
{
  "draw_white_space": ["all"],
  "ensure_newline_at_eof_on_save": true,
  "font_size": 12,
  "hot_exit": "disabled",
  "mde.auto_fold_link.enabled": false,
  "mde.auto_increment_ordered_list_number": false,
  "mde.list_indent_bullets": ["-"],
  "rulers": [120],
  "show_full_path": true,
  "tab_size": 2,
  "trim_trailing_white_space_on_save": "all",
  "word_wrap": false
}
```

## Fmt configuration

For [Fmt](https://github.com/mitranim/sublime-fmt), I use the following
configuration to reformat my Go files using [`gofmt`](https://pkg.go.dev/cmd/gofmt):

```json
{
  "rules": [
    {
      "selector": "source.go",
      "cmd": ["gofmt"],
      "format_on_save": true,
      "merge_type": "diff"
    }
  ]
}
```

## SublimeLinter configuration

For [SublimeLinter](http://www.sublimelinter.com/en/stable/), I add the following configuration:

```json
{
  "show_panel_on_save": "window",
  "lint_mode": "save"
}
```

## Configuration for SublimeLinter plugins

Configuration for SublimeLinter plugins.

### `SublimeLinter-contrib-markdownlint` configuration

For [`SublimeLinter-contrib-markdownlint`](https://github.com/jonlabelle/SublimeLinter-contrib-markdownlint),
I point the plugin to [`markdownlint-cli2`](https://github.com/DavidAnson/markdownlint-cli2)
instead of [`markdownlint-cli`](https://github.com/igorshubovych/markdownlint-cli):

```json
{
  "linters": {
    "markdownlint": {
      "executable": ["markdownlint-cli2"]
    }
  }
}
```

### `SublimeLinter-contrib-yamllint` configuration

For [`SublimeLinter-contrib-yamllint`](https://github.com/thomasmeeus/SublimeLinter-contrib-yamllint),
I avoid linting Markdown files that contain YAML code blocks:

```json
{
  "linters": {
    "yamllint": {
      "selector": "source.yaml - text.html.markdown"
    }
  }
}
```

### `SublimeLinter-eslint` configuration

For [`SublimeLinter-eslint`](https://github.com/SublimeLinter/SublimeLinter-eslint),
so that I can use `eslint` to also lint TOML files, I customize the `selector` setting:

```json
{
  "linters": {
    "eslint": {
      "selector": "source.toml, text.html.vue, source.js - meta.attribute-with-value"
    }
  }
}
```

TOML linting by using `eslint` is available with the [`eslint-toml-plugin`](https://ota-meshi.github.io/eslint-plugin-toml/).

### `SublimeLinter-rubocop` configuration

For [SublimeLinter-rubocop](https://github.com/SublimeLinter/SublimeLinter-rubocop),
so that I can use the RuboCop version specified by the project and so that I can use
configuration files outside the [standard configuration files locations](https://docs.rubocop.org/rubocop/configuration.html#config-file-locations),
I customize some settings:

```json
{
  "linters": {
    "rubocop": {
      // "args": ["--config", ".rubocop-gdk.yml"],
      "use_bundle_exec": true
    }
  }
}
```

Notably, in the
[`gitlab-development-kit` project](https://gitlab.com/gitlab-org/gitlab-development-kit),
RuboCop uses a [configuration file](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/.rubocop-gdk.yml)
outside the standard locations.

### `SublimeLinter-vale` configuration

For [`SublimeLinter-vale`](https://github.com/SublimeLinter/SublimeLinter-vale), I
customize the appearance of Vale suggestions:

```json
{
  "linters": {
    "vale": {
      "styles": [{
        "mark_style": "outline",
        "scope": "region.bluish",
        "types": ["suggestion"]
     }]
    }
  }
}
```
