---
title: 'Sublime Text packages'
showTableOfContents: true
summary: 'Sublime Text packages I use to extend Sublime Text.'
tags:
  - 'dev / dév'
---

<!-- vale french.french-lists-colons = NO -->
<!-- markdownlint-configure-file { "line-length": { "line_length": 120 } } -->
To extend Sublime Text, I use the following [Sublime Text packages](https://packagecontrol.io):

{{< sublime-text-packages >}}

## LSP for Sublime Text plugins

I use these [LSP for Sublime Text](https://lsp.sublimetext.io) plugins with
[extra configuration]({{< ref "sublime-text-configuration.md" >}}).

{{< lsp-for-sublime-text-plugins >}}

## Markdown Table Formatter customization

For [Markdown Table Formatter](https://github.com/bitwiser73/MarkdownTableFormatter), I use a
[feature branch](https://github.com/eread/MarkdownTableFormatter/tree/eread/add-support-for-variable-last-column-widths-in-markdown-tables)
on my [own fork](https://github.com/eread/MarkdownTableFormatter) to
[add support for variable last column widths in Markdown tables](https://github.com/bitwiser73/MarkdownTableFormatter/pull/23).

## SublimeLinter plugins

I use these [SublimeLinter](http://www.sublimelinter.com/en/stable/) plugins with
[extra configuration]({{< ref "sublime-text-configuration.md" >}}).

{{< sublimelinter-plugins >}}
