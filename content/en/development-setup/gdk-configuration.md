---
title: 'GDK configuration'
showTableOfContents: true
summary: 'My GitLab Development Kit configuration.'
tags:
  - 'dev / dév'
---

<!-- vale french.french-lists-colons = NO -->

How I configure
[GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) (GDK).

## GitLab Development Kit setup

For my GDK setup:

- I opt out of `asdf` and [opt in to `mise`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/mise.md).
- I opt out of `webpack` and [opt in to `vite`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/configuration.md#vite-settings).
- I configure `gdk.test` and `registry.test` as [local host names](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md).
- I configure GDK to [work over HTTPS](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/nginx.md).
- I configure GitLab Runner to [work in a Docker container](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md#executing-a-runner-from-within-docker).
- I configure the container registry to [work over HTTPS](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md).

## `gdk.yml` configuration

After having configured GDK, this is what my `gdk.yml` configuration file generally
looks like:

```yaml
---
asdf:
  opt_out: true
gdk:
  update_hooks:
    after:
      - cd gitlab && git checkout db/*
    before:
      - GDK_CLEANUP_CONFIRM=true gdk cleanup
hostname: gdk.test
https:
  enabled: true
mise:
  enabled: true
nginx:
  enabled: true
  ssl:
    certificate: <certificate>
    key: <key>
port: 3443
registry:
  enabled: true
  host: registry.test
  self_signed: true
  auth_enabled: true
  listen_address: 0.0.0.0
runner:
  enabled: true
  install_mode: docker
  executor: docker
  token: <token>
  extra_hosts:
    - gdk.test:172.16.123.1
    - registry.test:172.16.123.1
vite:
  enabled: true
webpack:
  enabled: false
```
