#!/usr/bin/env bash

LYCHEE_CMD=(lychee --include-fragments public)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "HTML link tests failed!")

gum log --time TimeOnly --structured --level info "Running HTML link tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running HTML link tests..." --show-error -- "${LYCHEE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${LYCHEE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "HTML link tests complete!"
