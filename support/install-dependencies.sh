#!/usr/bin/env bash

gum log --time TimeOnly --structured --level info "Installing dependencies..."

if ! gum spin --spinner dot --title "Updating mise if update available..." --show-error -- mise self-update --yes; then
  gum log --time TimeOnly --structured --level error "Updating mise failed!"
  exit 1
fi

if ! gum spin --spinner dot --title "Installing mise dependencies..." --show-error -- mise install --yes; then
  gum log --time TimeOnly --structured --level error "Installing mise dependencies failed!"
  exit 1
fi

eval "$(mise activate bash)"

if ! gum spin --spinner dot --title "Enabling corepack..." --show-error -- corepack enable; then
  gum log --time TimeOnly --structured --level error "Enabling corepack failed!"
  exit 1
fi

gum spin --spinner dot --title "Setting Yarn to latest stable version..." --show-error -- yarn set version stable
gum spin --spinner dot --title "Installing latest stable version of Yarn..." --show-error -- corepack install
gum spin --spinner dot --title "Removing node_modules directory ..." --show-error -- rm -rf node_modules
gum spin --spinner dot --title "Removing yarn.lock file ..." --show-error -- rm -f yarn.lock
gum spin --spinner dot --title "Checking for new Node.js package versions..." --show-error -- yarn dlx npm-check-updates --upgrade --packageManager yarn
gum spin --spinner dot --title "Installing latest Node.js packages..." --show-error -- yarn install
gum log --time TimeOnly --structured --level info "Dependencies installed!"
