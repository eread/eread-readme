#!/usr/bin/env bash

HADOLINT_CMD=(hadolint Dockerfile)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Dockerfile tests failed!")

gum log --time TimeOnly --structured --level info "Running Dockerfile tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running Dockerfile tests..." --show-error -- "${HADOLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${HADOLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Dockerfile tests complete!"
