#!/usr/bin/env bash

VHS_VALIDATE_CMD=(vhs validate cassette.tape)
VHS_BUILD_CMD=(vhs cassette.tape)
GIT_CMD=(git checkout -- project-tui.gif)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "GIF tests failed!")

gum log --time TimeOnly --structured --level info "Running GIF tests..."
go install
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Validating cassette.tape file..." --show-error -- "${VHS_VALIDATE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  elif ! gum spin --spinner dot --title "Testing if GIF is up to date..." --show-error -- "${VHS_BUILD_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  elif ! "${GIT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  else
    if ! git diff --exit-code support/menu-test-file.txt; then
      "${ERROR_CMD[@]}"
      exit 1
    fi
  fi
else
  if ! "${VHS_VALIDATE_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  elif ! "${VHS_BUILD_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  elif ! "${GIT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  else
    if ! git diff --exit-code support/menu-test-file.txt; then
      "${ERROR_CMD[@]}"
      exit 1
    fi
  fi
fi
gum log --time TimeOnly --structured --level info "GIF tests complete!"
