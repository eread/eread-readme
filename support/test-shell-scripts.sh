#!/usr/bin/env bash

SHELLCHECK_CMD=(shellcheck .lefthook/commit-msg/commitlint.sh support/*.sh)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Shell script tests failed!")

gum log --time TimeOnly --structured --level info "Running shell script tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running shell script tests..." --show-error -- "${SHELLCHECK_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${SHELLCHECK_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Shell script tests complete!"
