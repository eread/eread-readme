#!/usr/bin/env bash

BREW_BUNDLE_CMD=(brew bundle --no-lock)

if command -v gum > /dev/null; then
  gum spin --spinner dot --title "Installing Brew dependencies..." --show-error -- "${BREW_BUNDLE_CMD[@]}"
else
  "${BREW_BUNDLE_CMD[@]}"
fi

gum log --time TimeOnly --structured --level info "Brew dependencies installed!"
