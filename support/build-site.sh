#!/usr/bin/env bash

HUGO_CMD=()
GIT_CMD=(git submodule update --init)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "Building site failed!")

if [[ $1 == "site" ]]; then
  HUGO_CMD=(hugo --cleanDestinationDir --gc --minify --baseURL 'https://eread.gitlab.io/eread/')
elif [[ $1 == "pipeline" ]]; then
  if [[ -z $CI ]]; then
    gum log --time TimeOnly --structured --level error "Only use pipeline mode in a CI/CD pipeline!"
    exit 1
  fi
  HUGO_CMD=(hugo --cleanDestinationDir --gc --minify --baseURL "${CI_PAGES_URL}/${PAGES_PREFIX}")
elif [[ $1 == "pdf" ]]; then
  HUGO_CMD=(hugo --cleanDestinationDir --gc --minify --environment pdf)
else
  gum log --time TimeOnly --structured --level error "Specify a mode: site, pipeline, or pdf!"
  exit 1
fi

gum log --time TimeOnly --structured --level info "Building site..."
if [[ -z $CI ]]; then
  if [[ -z $(ls -A themes/blowfish) ]]; then
    gum log --time TimeOnly --structured --level info "Blowfish theme files not found..."
    gum spin --spinner dot --title "Updating Blowfish theme" --show-error -- "${GIT_CMD[@]}"
    gum log --time TimeOnly --structured --level info "Updating Blowfish theme complete!"
  fi
  if ! gum spin --spinner dot --title "Building site..." --show-error -- "${HUGO_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if [[ -z $(ls -A themes/blowfish) ]]; then
    gum log --time TimeOnly --structured --level info "Blowfish theme files not found..."
    "${GIT_CMD[@]}"
    gum log --time TimeOnly --structured --level info "Updating Blowfish theme complete!"
  fi
  if ! "${HUGO_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "Site build complete!"
