#!/usr/bin/env bash

gum log --time TimeOnly --structured --level info "Copying PDF..."

if [[ -e content/en/about-evan-read.pdf ]]; then
  if [[ -z $CI ]]; then
    gum spin --spinner dot --title "Copying local PDF..." --show-error -- cp content/en/about-evan-read.pdf public/about-me
  else
    cp content/en/about-evan-read.pdf public/about-me
  fi
else
  if [[ -z $CI ]]; then
    gum spin --spinner dot --title "Copying remote PDF..." --show-error -- curl --fail --location --output public/about-evan-read.pdf https://eread.gitlab.io/eread/about-me/about-evan-read.pdf
  else
    curl --fail --location --output public/about-evan-read.pdf https://eread.gitlab.io/eread/about-me/about-evan-read.pdf
  fi
fi

gum log --time TimeOnly --structured --level info "PDF copied!"
