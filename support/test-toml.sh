#!/usr/bin/env bash

ESLINT_CMD=(yarn run eslint mise.toml lychee.toml)
ERROR_CMD=(gum log --time TimeOnly --structured --level error "TOML tests failed!")

gum log --time TimeOnly --structured --level info "Running TOML tests..."
if [[ -z $CI ]]; then
  if ! gum spin --spinner dot --title "Running TOML tests..." --show-error -- "${ESLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
else
  if ! "${ESLINT_CMD[@]}"; then
    "${ERROR_CMD[@]}"
    exit 1
  fi
fi
gum log --time TimeOnly --structured --level info "TOML tests complete!"
