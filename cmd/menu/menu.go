package menu

import (
	"fmt"
	"os"

	"github.com/charmbracelet/bubbles/v2/viewport"
	tea "github.com/charmbracelet/bubbletea/v2"
	"github.com/charmbracelet/glamour"
	"github.com/charmbracelet/lipgloss/v2"
	"github.com/enescakir/emoji"
	"github.com/spf13/cobra"
	"gitlab.com/eread/eread/cmd"
)

type textStrings struct {
	headingTextEN     string
	headingTextFR     string
	instructionTextEN string
	instructionTextFR string
}

type model struct {
	choicesEN        []string
	choicesFR        []string
	selectedChoice   int
	display          viewport.Model
	showPercentage   bool
	selectedLanguage string
}

var text = textStrings{
	headingTextEN:     emoji.Parse("# README of Evan READ\n\nWhat would you like to do? :thinking:"),
	headingTextFR:     emoji.Parse("# Le LISEZ-MOI d'Evan Read\n\nQu'est ce que tu voudrais le faire ? :thinking:"),
	instructionTextEN: emoji.Parse("Press **a number** to display a file, use \u25B2 and \u25BC (or mouse wheel) to scroll, press **f** for French, or press **q** to quit. :wave:"),
	instructionTextFR: emoji.Parse("Appuie **un numéro** pour afficher un fichier, utilise \u25B2 et \u25BC (ou la molette de souris) pour faire défiler, appuie **e** pour l'anglais, ou appuie **q** pour quitter. :wave:"),
}

var menu = model{
	choicesEN:        []string{"  1. Read README.md", "  2. Read LIZEZ-MOI.md (in French)", "  3. Read CONTRIBUTING.md"},
	choicesFR:        []string{"  1. Lire README.md (en anglais)", "  2. Lire LIZEZ-MOI.md", "  3. Lire CONTRIBUTING.md (en anglais)"},
	selectedChoice:   0,
	showPercentage:   false,
	selectedLanguage: "",
}

var French bool

var menuCmd = &cobra.Command{
	Use:   "menu",
	Short: "Run the project menu",
	Long:  "Run the project terminal user interface",
	Run: func(cmd *cobra.Command, args []string) {
		if French {
			menu.selectedLanguage = "french"
		} else {
			menu.selectedLanguage = "english"
		}
		program := tea.NewProgram(menu, tea.WithAltScreen(), tea.WithMouseCellMotion())

		if _, err := program.Run(); err != nil {
			fmt.Printf("ERROR: %v", err)
			os.Exit(1)
		}
	},
}

func (m model) getMenu(language string) string {
	var choices []string
	choicesMenu := ""
	selectedChoiceStyle := lipgloss.NewStyle().Bold(true).Foreground(lipgloss.Color("63"))

	if language == "french" {
		choices = m.choicesFR
	} else {
		choices = m.choicesEN
	}

	for i := range choices {
		if m.selectedChoice == i+1 {
			choicesMenu += selectedChoiceStyle.Render(choices[i]) + "\n"
		} else {
			choicesMenu += choices[i] + "\n"
		}
	}
	return choicesMenu
}

func (m model) Init() tea.Cmd {
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	// Should align with line_length setting in .markdownlint-cli2.yaml
	displayWidth := 85
	displayHeight := 20
	displayStyle := lipgloss.NewStyle().BorderStyle(lipgloss.NormalBorder()).BorderForeground(lipgloss.Color("63"))

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "1":
			var cmd tea.Cmd
			var readmeContent string

			m.selectedChoice = 1

			readme, _ := os.ReadFile("README.md")
			m.display = viewport.New(viewport.WithWidth(displayWidth), viewport.WithHeight(displayHeight))
			m.display.Style = displayStyle
			readmeContent, _ = glamour.Render(string(readme), "dark")
			m.display.SetContent(readmeContent)
			m.showPercentage = false
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "2":
			var cmd tea.Cmd
			var lisez_moiContent string

			m.selectedChoice = 2

			lisez_moi, _ := os.ReadFile("LISEZ-MOI.md")
			m.display = viewport.New(viewport.WithWidth(displayWidth), viewport.WithHeight(displayHeight))
			m.display.Style = displayStyle
			lisez_moiContent, _ = glamour.Render(string(lisez_moi), "dark")
			m.display.SetContent(lisez_moiContent)
			m.showPercentage = false
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "3":
			var cmd tea.Cmd
			var contributingContent string

			m.selectedChoice = 3

			contributing, _ := os.ReadFile("CONTRIBUTING.md")
			m.display = viewport.New(viewport.WithWidth(displayWidth), viewport.WithHeight(displayHeight))
			m.display.Style = displayStyle
			contributingContent, _ = glamour.Render(string(contributing), "dark")
			m.display.SetContent(contributingContent)
			m.showPercentage = true
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "down":
			var cmd tea.Cmd
			m.display.LineDown(1)
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "up":
			var cmd tea.Cmd
			m.display.LineUp(1)
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "e":
			var cmd tea.Cmd
			m.selectedLanguage = "english"
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "f":
			var cmd tea.Cmd
			m.selectedLanguage = "french"
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		case "q":
			return m, tea.Quit
		}
	case tea.MouseMsg:
		switch msg.String() {
		case "wheeldown":
			var cmd tea.Cmd
			m.display.LineDown(1)
			m.display, cmd = m.display.Update(msg)
			return m, cmd

		case "wheelup":
			var cmd tea.Cmd
			m.display.LineUp(1)
			m.display, cmd = m.display.Update(msg)
			return m, cmd
		}
	}
	return m, nil
}

func (m model) View() string {
	footerStyle := lipgloss.NewStyle().Width(85).Bold(true).Align(lipgloss.Right).Foreground(lipgloss.Color("63"))
	textRenderer, _ := glamour.NewTermRenderer(
		glamour.WithStandardStyle("dark"),
		glamour.WithWordWrap(60),
	)
	var choicesMenu, heading, instructions string

	if m.selectedLanguage == "french" {
		heading = text.headingTextFR
		instructions = text.instructionTextFR
		choicesMenu = m.getMenu(m.selectedLanguage)
	} else {
		heading = text.headingTextEN
		instructions = text.instructionTextEN
		choicesMenu = m.getMenu(m.selectedLanguage)
	}

	heading, _ = textRenderer.Render(heading)
	instructions, _ = textRenderer.Render(instructions)
	content := m.display.View()
	footer := ""

	if m.showPercentage {
		footer = footerStyle.Render(fmt.Sprintf("%3.f%%", m.display.ScrollPercent()*100))
	}
	return fmt.Sprintf("%s%s%s%s\n%s", heading, choicesMenu, instructions, content, footer)
}

func init() {
	cmd.RootCmd.AddCommand(menuCmd)
	menuCmd.Flags().BoolVarP(&French, "french", "f", false, "Open menu in French instead of English")
}
