<!-- vale french.french-lists-colons = NO -->

# Contribute to the project

To contribute to the project, you must install some dependencies. To install dependencies:

1. Install [Homebrew](https://brew.sh).
1. Install [`mise`](https://mise.jdx.dev).
1. Run `make install`.

To install the `lefthook` pre-push Git configuration, run `make install-lefthook`.

## Changes to the project

Changes to the project fall into one of the following categories that do not
trigger a release:

- `chore`: Routine maintenance such as dependency updates. Never used for content or docs updates.
- `docs`: Changes to Markdown files in the project except for changes in:
  - `content/en/about-me-single-page.md`.
  - `assets/css/custom.css`.
- `feat`: Used for enhancements to the project. Never used for content or docs updates.
- `fix`: Used for fixes to the project. Never used for content or docs updates.

For changes to the PDF output of the project, use the `content` category. This category:

- Applies to changes to the `content/en/about-me-single-page.md` file, or any
  other changes, that affect the PDF output.
- Triggers a new release.

Specify the category of change you make to the project using
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

### Update project dependencies

To update the project's dependencies, run `make update`.

You can contribute any changes detected by Git through a merge request. Use the `chore` category.

## Preview changes locally

If you want to preview any changes, you can run one of the following:

- To preview locally, run `make build-preview`.
- To build the site, run `make build mode=site`.

## View the project menu

This project provides access to some tasks through a TUI menu. To open the menu, run `make menu`.

![Terminal user interface](./project-tui.gif)
