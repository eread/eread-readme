package main

import (
	"gitlab.com/eread/eread/cmd"
	_ "gitlab.com/eread/eread/cmd/menu"
	_ "gitlab.com/eread/eread/cmd/update"
)

func main() {
	cmd.Execute()
}
