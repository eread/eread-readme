MAKEFLAGS += --no-print-directory

.PHONY: all
all: clean test build

.PHONY: clean
clean:
	@support/clean.sh

install-lefthook:
	@gum log --time TimeOnly --structured --level info "Installing lefthook..."
	@gum spin --spinner dot --title "Installing lefthook..." --show-error -- yarn run lefthook install
	@gum log --time TimeOnly --structured --level info "lefthook installed!"

.PHONY: install
install:
	@gum log --time TimeOnly --structured --level info "Installing all dependencies..."
	@support/install-brew-dependencies.sh
	@support/install-dependencies.sh
	@gum log --time TimeOnly --structured --level info "All dependencies installed!"

sync-vale:
	@support/sync-vale.sh

update-dependencies:
	@go run main.go update
	@support/install-dependencies.sh

update: update-dependencies
	@gum log --time TimeOnly --structured --level info "Updating Blowfish theme..."
	@gum spin --spinner dot --title "Updating Blowfish theme..." --show-error -- git submodule update --remote --merge
	@gum log --time TimeOnly --structured --level info "Blowfish theme updated!"
	@support/update-go-modules.sh
	@gum log --time TimeOnly --structured --level info "Updates complete!"

create-pdf:
	@support/build-site.sh pdf
	@support/create-pdf.sh

test-css:
	@support/test-css.sh

test-dockerfile:
	@support/test-dockerfile.sh

test-gif:
	@support/test-gif.sh

test-go:
	@support/test-go.sh

test-makefile:
	@support/test-makefile.sh

test-markdown: sync-vale
	@support/test-markdown.sh

test-markdown-links:
	@support/test-markdown-links.sh

test-html-links:
	@support/build-site.sh pdf
	@support/test-html-links.sh

test-shell-scripts:
	@support/test-shell-scripts.sh

test-toml:
	@support/test-toml.sh

test-yaml:
	@support/test-yaml.sh

# All tests except GIF and PDF tests
.PHONY: test
test: clean test-css test-dockerfile test-go test-makefile test-markdown test-markdown-links test-html-links test-shell-scripts test-yaml
	@gum log --time TimeOnly --structured --level info "All tests complete!"

build-preview:
	@gum log --time TimeOnly --structured --level info "Starting Hugo server for preview..."
	@hugo server --cleanDestinationDir --disableFastRender

build-docker-image-amd64:
	@support/build-docker-image.sh amd64

build-docker-image-arm64:
	@support/build-docker-image.sh arm64

build:
	@support/build-site.sh $(mode)

menu:
	@go run main.go menu

build-gif:
	@support/build-gif.sh
