### Variables required for resolving base Docker image versions ###
ARG GO_VERSION=latest
ARG GOLANGCI_LINT_VERSION=latest
ARG GUM_VERSION=latest
ARG HADOLINT_VERSION=latest
ARG LYCHEE_VERSION=latest

### BEGIN go build stage ###
FROM golang:${GO_VERSION} AS go

ARG CHECKMAKE_VERSION

RUN go install "github.com/mrtazz/checkmake/cmd/checkmake@${CHECKMAKE_VERSION}"
### END go build stage ###

FROM golangci/golangci-lint:${GOLANGCI_LINT_VERSION} AS golangci-lint
FROM charmcli/gum:${GUM_VERSION} AS gum
FROM hadolint/hadolint:${HADOLINT_VERSION}-debian AS hadolint

### BEGIN hugo build stage ###
FROM debian:sid-slim AS hugo

ARG DEBIAN_FRONTEND=noninteractive
ARG TARGETARCH
ARG HUGO_VERSION

RUN apt-get update && apt-get install -y --no-install-recommends \
      ca-certificates=20241223 \
      curl=8.12* \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && curl --fail --location --output "hugo_${HUGO_VERSION}_linux-${TARGETARCH}.deb" "https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_linux-${TARGETARCH}.deb" \
 && dpkg -i "hugo_${HUGO_VERSION}_linux-${TARGETARCH}.deb" \
 && rm -f "hugo_${HUGO_VERSION}_linux-${TARGETARCH}.deb"
#### END hugo build stage ###

FROM lycheeverse/lychee:${LYCHEE_VERSION} AS lychee

### BEGIN node build stage ###
FROM debian:sid-slim AS node

ARG TARGETARCH
ARG NODE_VERSION=22.9.0

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && apt-get install -y --no-install-recommends \
      ca-certificates=20241223 \
      curl=8.12* \
      xz-utils=5.6* \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && if [[ ${TARGETARCH} == "amd64" ]]; then TARGETARCH="x64"; fi \
 && curl --fail --location --output "node-v${NODE_VERSION}-linux-${TARGETARCH}.tar.xz" "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-${TARGETARCH}.tar.xz" \
 && tar -xvf "node-v${NODE_VERSION}-linux-${TARGETARCH}.tar.xz" -C /usr/local \
 && mv "/usr/local/node-v${NODE_VERSION}-linux-${TARGETARCH}" /usr/local/node \
 && rm -f "node-v${NODE_VERSION}-linux-${TARGETARCH}.tar.xz"
### END node build stage ###

### BEGIN shellcheck build stage ###
FROM debian:sid-slim AS shellcheck

ARG DEBIAN_FRONTEND=noninteractive
ARG SHELLCHECK_VERSION

RUN apt-get update && apt-get install -y --no-install-recommends \
      "shellcheck=${SHELLCHECK_VERSION}-1" \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
#### END shellcheck build stage ###

### BEGIN vale build stage ###
FROM debian:sid-slim AS vale

ARG DEBIAN_FRONTEND=noninteractive
ARG TARGETARCH
ARG VALE_VERSION

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && apt-get install -y --no-install-recommends \
      ca-certificates=20241223 \
      curl=8.12* \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && if [[ ${TARGETARCH} == "amd64" ]]; then TARGETARCH="64-bit"; fi \
 && curl --fail --location --output vale_${VALE_VERSION}_Linux_${TARGETARCH}.tar.gz https://github.com/errata-ai/vale/releases/download/v${VALE_VERSION}/vale_${VALE_VERSION}_Linux_${TARGETARCH}.tar.gz \
 && tar -zxvf vale_${VALE_VERSION}_Linux_${TARGETARCH}.tar.gz --directory /usr/local/bin vale \
 && rm -f vale_${VALE_VERSION}_Linux_${TARGETARCH}.tar.gz
### END vale build stage ###

### BEGIN build stage of Docker image to publish ###
FROM debian:sid-slim

COPY --from=go /go/bin/checkmake /usr/local/bin/checkmake
COPY --from=go /usr/local/go /usr/local/go
COPY --from=golangci-lint /usr/bin/golangci-lint /usr/local/bin/golangci-lint
COPY --from=gum /usr/local/bin/gum /usr/local/bin/gum
COPY --from=hadolint /bin/hadolint /usr/local/bin/hadolint
COPY --from=hugo /usr/local/bin/hugo /usr/local/bin/hugo
COPY --from=lychee /usr/local/bin/lychee /usr/local/bin/lychee
COPY --from=node /usr/local/node /usr/local/node
COPY --from=shellcheck /usr/bin/shellcheck /usr/local/bin/shellcheck
COPY --from=vale /usr/local/bin/vale /usr/local/bin/vale

ARG DEBIAN_FRONTEND=noninteractive
ARG MARKDOWNLINT_CLI2_VERSION
ARG YAMLLINT_VERSION

ENV PATH=/usr/local/go/bin:/usr/local/node/bin:$PATH

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update && apt-get install -y --no-install-recommends \
      ca-certificates=20241223 \
      git=1:2.47* \
      make=4.4* \
      yamllint=${YAMLLINT_VERSION}* \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && npm install markdownlint-cli2@${MARKDOWNLINT_CLI2_VERSION} --global \
 && corepack enable
### END build stage of Docker image to publish ###
