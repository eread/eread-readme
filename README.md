<!-- vale french.french-lists-colons = NO -->

# README of Evan Read

This README provides some information on me and my development environment.

- See the [published version](https://eread.gitlab.io/eread/).
- See the [Markdown version](https://gitlab.com/eread/eread/-/tree/main/content/en).

The project's documentation [is available](./CONTRIBUTING.md#view-the-project-menu)
through a terminal user interface.

[Lire en français](./LISEZ-MOI.md).
